# -*- coding: utf-8 -*-
"""
Developed by Arseniy Sluchevskiy <arseniy.sluchevskiy@gmail.com>
"""
import os
import sys
import logging
import locale
import optparse
from spiders import *
try:
    from local_settings import SETTINGS
except ImportError:
    from settings import SETTINGS

# hack for unicode console i/o
reload(sys)
sys.setdefaultencoding(locale.getpreferredencoding())

parser = optparse.OptionParser("usage: %prog [options]")
parser.add_option('-d', '--debug', action="store_true", dest="debug", default=SETTINGS.get('debug'), help='enable debug logging')
parser.add_option('-m', '--mode', action="store", dest="mode", default='parse', help='mode: parse or export')
(options, args) = parser.parse_args()

if options.debug:
    logging.basicConfig(level=logging.DEBUG)
else:
    logging.basicConfig(level=logging.INFO)

spiders_list = (AromaticaSpider, SandaldomSpider, NkoronaSpider,
                AromajazzSpider, SanatteaSpider, PrirodavSpider,
                AvvallonSpider, PeropavlinaSpider, VesbookSpider)
#spiders_list = (SandaldomSpider, )

if options.mode == 'parse':
    for spider in spiders_list:
        p = spider(thread_number=SETTINGS['threads'])
        p.setup_settings(SETTINGS)
        if SETTINGS.get('use_cache'):
            p.setup_cache(database=SETTINGS['cache_db'])
        logging.info('Running %s spider...' % p.site_name)
        p.run()
elif options.mode == 'export':
    ExportItems(SETTINGS).export_csv()
