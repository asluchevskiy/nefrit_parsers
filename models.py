# -*- coding: utf-8 -*-
"""
Developed by Arseniy Sluchevskiy <arseniy.sluchevskiy@gmail.com>
"""

import datetime
from django.conf import settings
try:
    from local_settings import DATABASES, STANDALONE_APP_NAME
except ImportError:
    from settings import DATABASES, STANDALONE_APP_NAME

settings.configure(
    DATABASES=DATABASES,
    STANDALONE_APP_NAME=STANDALONE_APP_NAME,
)

from django.db import models

class StandAloneMetaclass(models.base.ModelBase):
    """Метакласс, который задает app_label моделям используя
    значение из STANDALONE_APP_NAME"""
    def __new__(cls, name, bases, attrs):
        app_name = settings.STANDALONE_APP_NAME or None
        if app_name:
            if 'Meta' in attrs:
                if not hasattr(attrs['Meta'], 'app_label'):
                    # устанавливаем app_label только если он уже не указан в самой модели
                    setattr(attrs['Meta'], 'app_label', app_name)
        return super(StandAloneMetaclass, cls).__new__(cls, name, bases, attrs)

class File(models.Model):
    __metaclass__ = StandAloneMetaclass
#    id = models.IntegerField(primary_key=True, db_column='ID')
    timestamp_x = models.DateTimeField(db_column='TIMESTAMP_X', default=datetime.datetime.now())
    module_id = models.CharField(max_length=150, db_column='MODULE_ID', blank=True, default='iblock')
    height = models.IntegerField(null=True, db_column='HEIGHT', blank=True)
    width = models.IntegerField(null=True, db_column='WIDTH', blank=True)
    file_size = models.IntegerField(db_column='FILE_SIZE')
    content_type = models.CharField(max_length=765, db_column='CONTENT_TYPE', blank=True)
    subdir = models.CharField(max_length=765, db_column='SUBDIR', blank=True)
    file_name = models.CharField(max_length=765, db_column='FILE_NAME')
    original_name = models.CharField(max_length=765, db_column='ORIGINAL_NAME', blank=True)
    description = models.CharField(max_length=765, db_column='DESCRIPTION', blank=True)
    handler_id = models.CharField(max_length=150, db_column='HANDLER_ID', blank=True, default=None)
    class Meta:
        db_table = u'b_file'

class Element(models.Model):
    __metaclass__ = StandAloneMetaclass
#    id = models.IntegerField(primary_key=True, db_column='ID')
    timestamp_x = models.DateTimeField(null=True, db_column='TIMESTAMP_X', blank=True, default=datetime.datetime.now())
    modified_by = models.IntegerField(null=True, db_column='MODIFIED_BY', blank=True, default=1)
    date_create = models.DateTimeField(null=True, db_column='DATE_CREATE', blank=True, default=datetime.datetime.now())
    created_by = models.IntegerField(null=True, db_column='CREATED_BY', blank=True, default=1)
    iblock_id = models.IntegerField(db_column='IBLOCK_ID', default=2)
    iblock_section_id = models.IntegerField(null=True, db_column='IBLOCK_SECTION_ID', blank=True)
    active = models.CharField(max_length=3, db_column='ACTIVE', default='Y')
    active_from = models.DateTimeField(null=True, db_column='ACTIVE_FROM', blank=True)
    active_to = models.DateTimeField(null=True, db_column='ACTIVE_TO', blank=True)
    sort = models.IntegerField(db_column='SORT', default=500)
    name = models.CharField(max_length=765, db_column='NAME')
    preview_picture = models.IntegerField(null=True, db_column='PREVIEW_PICTURE', blank=True)
    preview_text = models.TextField(db_column='PREVIEW_TEXT', blank=True)
    preview_text_type = models.CharField(max_length=12, db_column='PREVIEW_TEXT_TYPE', default='html')
    detail_picture = models.IntegerField(null=True, db_column='DETAIL_PICTURE', blank=True)
    detail_text = models.TextField(db_column='DETAIL_TEXT', blank=True)
    detail_text_type = models.CharField(max_length=12, db_column='DETAIL_TEXT_TYPE', default='html')
    searchable_content = models.TextField(db_column='SEARCHABLE_CONTENT', blank=True)
    wf_status_id = models.IntegerField(null=True, db_column='WF_STATUS_ID', blank=True, default=1)
    wf_parent_element_id = models.IntegerField(null=True, db_column='WF_PARENT_ELEMENT_ID', blank=True)
    wf_new = models.CharField(max_length=3, db_column='WF_NEW', blank=True)
    wf_locked_by = models.IntegerField(null=True, db_column='WF_LOCKED_BY', blank=True)
    wf_date_lock = models.DateTimeField(null=True, db_column='WF_DATE_LOCK', blank=True)
    wf_comments = models.TextField(db_column='WF_COMMENTS', blank=True)
    in_sections = models.CharField(max_length=3, db_column='IN_SECTIONS', default='Y')
    xml_id = models.CharField(max_length=765, db_column='XML_ID', blank=True)
    code = models.CharField(max_length=765, db_column='CODE', blank=True)
    tags = models.CharField(max_length=765, db_column='TAGS', blank=True)
    tmp_id = models.CharField(max_length=120, db_column='TMP_ID', blank=True)
    wf_last_history_id = models.IntegerField(null=True, db_column='WF_LAST_HISTORY_ID', blank=True)
    show_counter = models.IntegerField(null=True, db_column='SHOW_COUNTER', blank=True)
    show_counter_start = models.DateTimeField(null=True, db_column='SHOW_COUNTER_START', blank=True)
    class Meta:
        db_table = u'b_iblock_element'

class ElementProperty(models.Model):
    __metaclass__ = StandAloneMetaclass
#    id = models.IntegerField(primary_key=True, db_column='ID')
    iblock_property_id = models.IntegerField(db_column='IBLOCK_PROPERTY_ID')
    iblock_element_id = models.IntegerField(db_column='IBLOCK_ELEMENT_ID')
    value = models.TextField(db_column='VALUE')
    value_type = models.CharField(max_length=12, db_column='VALUE_TYPE', default='text')
    value_enum = models.IntegerField(null=True, db_column='VALUE_ENUM', blank=True)
    value_num = models.DecimalField(decimal_places=4, null=True, max_digits=20, db_column='VALUE_NUM', blank=True)
    description = models.CharField(max_length=765, db_column='DESCRIPTION', blank=True, null=True)
    class Meta:
        db_table = u'b_iblock_element_property'

class Section(models.Model):
    __metaclass__ = StandAloneMetaclass
#    id = models.IntegerField(primary_key=True, db_column='ID')
    timestamp_x = models.DateTimeField(db_column='TIMESTAMP_X', default=datetime.datetime.now())
    modified_by = models.IntegerField(null=True, db_column='MODIFIED_BY', blank=True, default=1)
    date_create = models.DateTimeField(null=True, db_column='DATE_CREATE', blank=True, default=datetime.datetime.now())
    created_by = models.IntegerField(null=True, db_column='CREATED_BY', blank=True, default=1)
    iblock_id = models.IntegerField(db_column='IBLOCK_ID', default=2)
    iblock_section_id = models.IntegerField(null=True, db_column='IBLOCK_SECTION_ID', blank=True)
    active = models.CharField(max_length=3, db_column='ACTIVE', default='Y')
    global_active = models.CharField(max_length=3, db_column='GLOBAL_ACTIVE', default='Y')
    sort = models.IntegerField(db_column='SORT', default=500)
    name = models.CharField(max_length=765, db_column='NAME')
    picture = models.IntegerField(null=True, db_column='PICTURE', blank=True)
    left_margin = models.IntegerField(null=True, db_column='LEFT_MARGIN', blank=True, default=1)
    right_margin = models.IntegerField(null=True, db_column='RIGHT_MARGIN', blank=True, default=2)
    depth_level = models.IntegerField(null=True, db_column='DEPTH_LEVEL', blank=True)
    description = models.TextField(db_column='DESCRIPTION', blank=True)
    description_type = models.CharField(max_length=12, db_column='DESCRIPTION_TYPE')
    searchable_content = models.TextField(db_column='SEARCHABLE_CONTENT', blank=True)
    code = models.CharField(max_length=765, db_column='CODE', blank=True)
    xml_id = models.CharField(max_length=765, db_column='XML_ID', blank=True)
    tmp_id = models.CharField(max_length=120, db_column='TMP_ID', blank=True)
    detail_picture = models.IntegerField(null=True, db_column='DETAIL_PICTURE', blank=True)
    socnet_group_id = models.IntegerField(null=True, db_column='SOCNET_GROUP_ID', blank=True)
    class Meta:
        db_table = u'b_iblock_section'

class SectionElement(models.Model):
    __metaclass__ = StandAloneMetaclass
    iblock_section_id = models.IntegerField(db_column='IBLOCK_SECTION_ID')
    iblock_element_id = models.IntegerField(db_column='IBLOCK_ELEMENT_ID', primary_key=True)
    additional_property_id = models.IntegerField(unique=True, null=True, db_column='ADDITIONAL_PROPERTY_ID', blank=True)
    class Meta:
        db_table = u'b_iblock_section_element'

def create_tables(tables):
    from django.db import connection, transaction
    from django.core.management.color import no_style
    pending_references = {}  # список внешних колючей, ожидающих создания связываемых таблиц
    seen_models = set()  # список созданных
    style = no_style()  # указывает, что не нужно раскрашивать синтаксис сгенерированного SQL
    for model in tables:  # проходим по списку моделей
        sql, references = connection.creation.sql_create_model(model, style) # генерируем SQL и объекты внешних ключей
        seen_models.add(model)
        for refto, refs in references.items():
            pending_references.setdefault(refto, []).extend(refs)
            if refto in seen_models:
                sql.extend(  # если все таблицы внешнего ключа существуют, то добавляем сам ключ
                    connection.creation.sql_for_pending_references(
                            refto,
                            style,
                            pending_references))
        sql.extend(
            connection.creation.sql_for_pending_references(
                model,
                style,
                pending_references))
        cursor = connection.cursor()
        for stmt in sql:  # выполняем запросы к БД
            print stmt
            try:
                cursor.execute(stmt)
            except Exception, e:
                print e
                pass
        print
    transaction.commit_unless_managed()  # завершаем транзакцию
    for model in tables:  # создаем индексы
        index_sql = connection.creation.sql_indexes_for_model(model, style)
        if index_sql:
            try:
                for stmt in index_sql:
                    print stmt
                    cursor.execute(stmt)
            except Exception, e:
                transaction.rollback_unless_managed()
            else:
                transaction.commit_unless_managed()

if __name__ == "__main__":
    # находим все классы Django моделей в локальной области видимости и создаем для них
    # таблицы в БД
    create_tables(mymodel for mymodel in locals().values() if type(mymodel) == StandAloneMetaclass)
