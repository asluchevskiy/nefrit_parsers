# -*- coding: utf-8 -*-
"""
Developed by Arseniy Sluchevskiy <arseniy.sluchevskiy@gmail.com>
"""
import os

SETTINGS = {
    'threads': 16,
    'debug' : True,
#    'proxy_list' : os.path.join(os.path.dirname(__file__),'ru_proxy.list'),
    'use_cache' : False,
#    'use_cache' : True,
    'cache_db' : 'nefrit',
    'images_dir' : '/drive1/work/nefrit/',
    'image_thumb_size' : (100, 100),
    'jpeg_quality' : 90,
    'csv_dir' : '/drive1/work/nefrit-csv/',
    'tmp_dir' : '/tmp/',
}

DATABASES={
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'nefrit',
        'USER': 'nefrit',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    }
},

STANDALONE_APP_NAME='nefrit'