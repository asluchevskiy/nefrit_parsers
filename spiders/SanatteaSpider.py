# -*- coding: utf-8 -*-
"""
Developed by Arseniy Sluchevskiy <arseniy.sluchevskiy@gmail.com>
"""

import re
from grab.spider import Task
from grab.tools.text import normalize_space, find_number
from lxml.html.clean import Cleaner
from BaseSpider import BaseSpider
from mapping import MAPPING

class SanatteaSpider(BaseSpider):
    initial_urls = ['http://www.sanattea.ru/']
    site_name = 'sanattea.ru'

    def task_initial(self, grab, task):
        pq = grab.pyquery
        for a in pq.find('a.rcat_root_category'):
            cat_name = a.text
            if cat_name in MAPPING[self.site_name]:
                yield Task('list', url=grab.make_url_absolute(a.get('href'))+'all/', category = [cat_name])

    def task_list(self, grab, task):
        pq = grab.pyquery
        for a in pq.find('div.prdbrief_name a'):
            url = grab.make_url_absolute(a.get('href'))
            item = self.db.item.find_one({'url':url, 'site':self.site_name})
            if not item:
                yield Task('page', url=url, category=task.category)
            elif not item['is_img_downloaded']:
                yield Task('image', id=item['_id'], url=item['img_url'], disable_cache=True)

    def task_page(self, grab, task):
        pq = grab.pyquery
        data = self.get_data(task)
        data['name'] =  pq.find('h1').text()
        data['description'] = pq.find('div.cpt_product_description div').remove('a').outerHtml()
        data['description'] = normalize_space(Cleaner(style=True).clean_html(data['description']))
        data['description'] = re.sub(' *class=["\'].+?["\']', '', data['description'])
        data['img_url'] = pq.find('div.cpt_product_images div div a').attr('href')
        data['article'] = None
        if not data['img_url']:
            data['img_url'] = pq.find('img#img-current_picture').attr('src')
        if data['img_url']:
            data['img_url'] = grab.make_url_absolute(data['img_url'])
        else:
            data['is_img_downloaded'] = True
        try:
            data['price'] = float(pq.find('span.totalPrice').text().split()[0])
        except Exception:
            data['price'] = None
        _id = self.db.item.save(data)
        if data['img_url']:
            yield Task('image', id=_id, url=data['img_url'], disable_cache=True)
