# -*- coding: utf-8 -*-
"""
Developed by Arseniy Sluchevskiy <arseniy.sluchevskiy@gmail.com>
"""

import datetime
import string
import re
import os
import zipfile
import xlrd
import shutil
from grab.spider import Task
from grab.tools.text import normalize_space
from BaseSpider import BaseSpider
from mapping import MAPPING

class SandaldomSpider(BaseSpider):
    initial_urls = ['http://sandaldom.ru']
    site_name = 'sandaldom.ru'

    price_data = dict()

    def task_initial(self, grab, task):
        yield Task('price', url='http://sandaldom.ru/catalog/price/price.zip')

    def task_catalog(self, grab, task):

        def get_name(name):
            return re.sub('\(\d+\)', '', name).strip()

        def go_category_list(object, parent=[]):
            children_list = object.children()
            categories = []
            for i in range(len(children_list)):
                if children_list[i].tag == 'li':
                    categories = parent + [get_name( children_list.eq(i).text() )]
                    key = string.join(categories, '; ')
                    if MAPPING[self.site_name].has_key(key):
                        url = grab.make_url_absolute( children_list.eq(i).find('a').attr('href') )
                        self.add_task( Task('list', url=url+'?SHOWALL_1=1', category=categories) )
                if children_list[i].tag == 'ul':
                    go_category_list(children_list.eq(i), parent=categories)

        pq = grab.pyquery
        div_list = pq.find('td[style="background:#cc9f68;"]').children('div').find('div')
        for i in range(len(div_list)):
            if not div_list.eq(i).children('ul'):
                continue
            go_category_list(div_list.eq(i).children('ul'))

    def task_list(self, grab, task):
        pq = grab.pyquery
        for a in pq.find('div.catalog-top div').children('a[style="color:#000000;"]'):
            url = grab.make_url_absolute(a.get('href'))
            item = self.db.item.find_one({'url':url, 'site':self.site_name})
            if not item:
                yield Task('page', url=url, category=task.category)
            elif not item['is_img_downloaded']:
                yield Task('image', id=item['_id'], url=item['img_url'], disable_cache=True)

    def task_page(self, grab, task):
        pq = grab.pyquery
        data = dict()
        data['ctime'] = datetime.datetime.now()
        data['category'] = task.category
        data['site'] = self.site_name
        data['url'] = task.url
        data['img_url'] = grab.make_url_absolute(pq.find('div.catalog-element img[align=left]').attr('src'))
        data['name'] = pq.find('div.catalog-element b[style="font-size:22px;"]').text()
        data['article'] = pq.find('div.catalog-element table tr td').children('b').eq(1).text()
        data['description'] = pq.find('div.catalog-element table tr td').children('p').html()
        data['description'] = normalize_space(data['description'].strip()) if data['description'] else ''
        if self.price_data.has_key(data['article']):
            data['price'] = self.price_data[data['article']]
        else:
            data['price'] = None
        data['is_parsed'] = True
        data['is_exported'] = False
        data['is_img_downloaded'] = False
        _id = self.db.item.save(data)
        yield Task('image', id=_id, url=data['img_url'], disable_cache=True)

    def task_price(self, grab, task):
        zip_path = os.path.join(self.settings['tmp_dir'], 'price.zip')
        grab.response.save(zip_path)

        with zipfile.ZipFile(zip_path, 'r') as z:
            arch_name = z.namelist()[0]
            z.extractall( os.path.join(self.settings['tmp_dir'], 'nefrit-price') )

            xls_old_path = os.path.join(self.settings['tmp_dir'], 'nefrit-price', arch_name)
            xls_path = os.path.join(self.settings['tmp_dir'], 'nefrit-price', 'price.xls')
            shutil.move(xls_old_path, xls_path)

            rb =xlrd.open_workbook(xls_path)
            sheet = rb.sheet_by_index(0)
            for rownum in range(sheet.nrows):
                row = sheet.row_values(rownum)
                if type(row[2]) == unicode:# and row[3] is float:
                    self.price_data[ row[2].strip()] = row[3]
            shutil.rmtree(os.path.dirname(xls_path))
        os.remove(zip_path)

        yield Task('catalog', url='http://sandaldom.ru/catalog/')
