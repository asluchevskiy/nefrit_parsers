# -*- coding: utf-8 -*-
"""
Developed by Arseniy Sluchevskiy <arseniy.sluchevskiy@gmail.com>
"""

import re
from grab.spider import Task
from grab.tools.text import normalize_space, find_number
from lxml.html import tostring
from BaseSpider import BaseSpider
from mapping import MAPPING

class VesbookSpider(BaseSpider):
    initial_urls = ['http://vesbook.ru/catalog/']
    site_name = 'vesbook.ru'

    def task_initial(self, grab, task):
        pq = grab.pyquery
        for a in pq.find('div#rubricmenu a'):
            cat_name = normalize_space(a.text)
            if cat_name in MAPPING[self.site_name].keys():
                url = grab.make_url_absolute('/'+a.get('href'))
                yield Task('list', url=url, category = [cat_name])

    def task_list(self, grab, task):
        pq = grab.pyquery
        for a in pq.find('div.blockLine h2 a'):
            url = grab.make_url_absolute('/'+a.get('href'))
            item = self.db.item.find_one({'url':url, 'site':self.site_name})
            if not item:
                yield Task('page', url=url, category=task.category)
            elif not item['is_img_downloaded']:
                yield Task('image', id=item['_id'], url=item['img_url'], disable_cache=True)

    def task_page(self, grab, task):
        pq = grab.pyquery
        data = self.get_data(task)
        data['name'] = pq.find('div.detailsblock h2').text()
        data['img_url'] = grab.make_url_absolute( '/'+pq.find('div.bigcover img').attr('src') )
        data['article'] = None
        data['price'] = None
        descr_prop = ''
        for p in pq.find('div.bigcover p'):
            prop = p.text_content().strip()
            if prop == u'Формат':
                continue
            s = re.search(u'Цена ([\d\.]+)', prop)
            if s:
                data['price'] = float(s.group(1))
            else:
                descr_prop += u'<p>%s</p>' % prop
        for a in pq.find('div.bookdetails a'):
            a.tag = 'span'
            if 'href' in a.attrib:
                del a.attrib['href']
        descr = ''
        for div in pq.find('div.bookdetails'):
             descr += tostring(div, encoding='utf-8')
        data['description'] = self.clean_html( '<div>%s</div>\n%s' % (descr_prop, descr) )
        _id = self.db.item.save(data)
        yield Task('image', id=_id, url=data['img_url'], disable_cache=True)
