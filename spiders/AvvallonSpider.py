# -*- coding: utf-8 -*-
"""
Developed by Arseniy Sluchevskiy <arseniy.sluchevskiy@gmail.com>
"""

import datetime
import re
from grab.spider import Task
from grab.tools.text import normalize_space, find_number
from BaseSpider import BaseSpider
from mapping import MAPPING

class AvvallonSpider(BaseSpider):
    initial_urls = ['http://www.avvallon.ru']
    site_name = 'avvallon.ru'

    def task_initial(self, grab, task):
        yield Task('taro', url='http://www.avvallon.ru/taro.html', category=[u'Таро', u'Карты Таро'])
        yield Task('taro', url='http://www.avvallon.ru/podarochnyie.html', category=[u'Таро', u'Подарочные наборы'])
        yield Task('taro', url='http://www.avvallon.ru/orakulyi.html', category=[u'Таро', u'Оракулы'])
        yield Task('cards', url='http://www.avvallon.ru/cards.html', category=[u'Карты'])
        yield Task('accessory', url='http://www.avvallon.ru/accessoirerunes.html', category=[u'Руны'])
        yield Task('accessory', url='http://www.avvallon.ru/accessoireclothes.html', category=[u'Скатерти для гадания'])

    def task_taro(self, grab, task):
        pq = grab.pyquery
        tr_list = pq.find('table[width=850]').eq(0).find('tr')
        for i in range(len(tr_list)):
            tr = tr_list.eq(i)
            name = normalize_space( tr.find('strong').eq(0).text() )
            description = self.clean_html( tr.find('td[width=450]').remove('a').html() )
            img_url = grab.make_url_absolute( tr.find('td[width=350]').children('a').attr('href') )
            self.__save_item(dict(name=name, description=description, img_url=img_url, category=task.category))

    def task_cards(self, grab, task):
        pq = grab.pyquery
        td_list = pq.find('td[width=400]')
        for i in range(len(td_list)):
            if not td_list.eq(i).text():
                continue
            s = re.search('(.+?) \((.+?)\)', td_list.eq(i).text())
            if not s:
                continue
            article = s.group(1)
            name = s.group(2)
            img_url = grab.make_url_absolute( td_list.eq(i).children('a').attr('href') )
            self.__save_item(dict(name=name, article=article, img_url=img_url, category=task.category))

    def task_accessory(self, grab, task):
        pq = grab.pyquery
        td_list = pq.find('table[width=850] td[valign=top]')
        for i in range(len(td_list)):
            td = td_list.eq(i)
            if not td.text():
                continue
            img_url = grab.make_url_absolute( td.children('a').attr('href') )
            raw_price = td.children('strong').eq(1).text()
            price = float(find_number(raw_price)) if raw_price else None
            s = re.search('\((.+?)\)', td.text())
            name = s.group(1) if s else td.text()
            article = td.children('strong').eq(0).text() if s else None
            self.__save_item(dict(name=name, img_url=img_url, price=price, article=article, category=task.category))

    def __save_item(self, item_data):
        data = dict(ctime=datetime.datetime.now(), site=self.site_name, description='',
            is_parsed=True, is_exported=False, is_img_downloaded=False, article=None, price=None)
        data.update(item_data)
        item = self.db.item.find_one({'name':data['name'], 'site':self.site_name, 'article':data['article']})
        if not item:
            _id = self.db.item.save(data)
            self.add_task( Task('image', id=_id, url=data['img_url'], disable_cache=True) )
        elif not item['is_img_downloaded']:
            self.add_task( Task('image', id=item['_id'], url=item['img_url'], disable_cache=True) )
