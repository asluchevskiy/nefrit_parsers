# -*- coding: utf-8 -*-
"""
Developed by Arseniy Sluchevskiy <arseniy.sluchevskiy@gmail.com>
"""

import datetime
import logging
from urlparse import parse_qs, urlparse
from grab.spider import Task
from grab.tools.text import normalize_space
from BaseSpider import BaseSpider

class AromaticaSpider(BaseSpider):
    initial_urls = ['http://www.aromatika.su/']
    site_name = 'aromatika.su'

    def task_initial(self, grab, task):
        pq = grab.pyquery
        for a in pq.find('table.box_body_table li.bg_list a'):
            yield Task('list', url=a.get('href'), category=[a.text])

    def task_list(self, grab, task):
        pq = grab.pyquery
        # subcategories
        a_list = pq.find('table.box_body_table li.bg_list_sub a')
        if a_list and len(task.category)==1:
            for a in a_list:
                yield Task('list', url=a.get('href'), category=task.category+[a.text])
            return
        # pagination
        a = pq.find('a.pageResults[title=" Следующая страница "]').eq(0)
        if a:
            yield Task('list', url=a.attr('href'), category=task.category)
        # items
        for a in pq.find('td.vam span a'):
            url = a.get('href')
            site_id = parse_qs(url)['products_id'][0]
            item = self.db.item.find_one({'site':self.site_name, 'site_id':site_id})
            if not item:
                yield Task('item', url=url, site_id=site_id, category=task.category)
            elif not item['is_img_downloaded']:
                yield Task('image', id=item['_id'], url=item['img_url'], disable_cache=True)

    def task_item(self, grab, task):
        pq = grab.pyquery
        data = dict()
#        data['mtime'] = data['ctime'] = datetime.datetime.now()
        data['ctime'] = datetime.datetime.now()
        data['category'] = task.category
        data['site_id'] = task.site_id
        data['url'] = task.url
        data['site'] = self.site_name
        data['name'] = pq.find('table.title_info em')[0].text
        data['article'] = pq.find('table.title_info span.smallText').text().strip('[]')
        if not pq.find('span.productSpecialPrice span.productSpecialPrice'):
            raw_price = pq.find('span.productSpecialPrice').text()
        else:
            raw_price = pq.find('span.productSpecialPrice span.productSpecialPrice').text()
        data['price'] = float(raw_price.strip('руб.').replace(' ','').replace('\'',''))
        description = pq.find('div.padd3').remove('span.productSpecialPrice').remove('br[style="line-height:11px"]').html()
        data['description'] = normalize_space(description.replace('&#13;',''))
        data['img_url'] = pq.find('table.table_pic_width a').attr('href')
        data['is_parsed'] = True
        data['is_exported'] = False
        data['is_img_downloaded'] = False
        _id = self.db.item.save(data)
        yield Task('image', id=_id, url=data['img_url'], disable_cache=True)

