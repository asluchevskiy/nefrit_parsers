# -*- coding: utf-8 -*-
"""
Developed by Arseniy Sluchevskiy <arseniy.sluchevskiy@gmail.com>
"""

import datetime
import string
from grab.spider import Task
from grab.tools.text import normalize_space
from BaseSpider import BaseSpider
from mapping import MAPPING

class NkoronaSpider(BaseSpider):
    initial_urls = ['http://www.nkorona.ru/production/production.html']
    site_name = 'nkorona.ru'

    def task_initial(self, grab, task):
        pq = grab.pyquery
        for a in pq.find('p.products1 a'):
            cat_name = a.text_content().strip()
            if cat_name in MAPPING[self.site_name].keys():
                url = a.get('href').replace('HTTP://', 'http://')
                yield Task('list', url=url, category=[cat_name])

    def task_list(self, grab, task):
        pq = grab.pyquery
        li_list = pq.find('div."block dark clearfix" ul li')
        for i in range(len(li_list)):
            href = li_list.eq(i).find('a').eq(0).attr('href')
            if not href:
                continue
            url = grab.make_url_absolute(href).rsplit('#', 1)[0]
            item = self.db.item.find_one({'url':url, 'site':self.site_name})
            if not item:
                yield Task('page', url=url, category=task.category)
            elif not item['is_img_downloaded']:
                yield Task('image', id=item['_id'], url=item['img_url'], disable_cache=True)

    def task_page(self, grab, task):
        pq = grab.pyquery
        data = dict()
        data['ctime'] = datetime.datetime.now()
        data['category'] = task.category
        data['site'] = self.site_name
        data['url'] = task.url
        data['name'] = pq.find('div."block dark clearfix" h1').text()
        data['img_url'] = grab.make_url_absolute( pq.find('img.fleft').attr('src') )
        data['article'] = None
        data['price'] = None
        data['description'] = normalize_space( pq.find('div."block dark clearfix" ul li').remove('img').html() )
        data['is_parsed'] = True
        data['is_exported'] = False
        data['is_img_downloaded'] = False
        _id = self.db.item.save(data)
        yield Task('image', id=_id, url=data['img_url'], disable_cache=True)
