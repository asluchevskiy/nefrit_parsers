# -*- coding: utf-8 -*-
"""
Developed by Arseniy Sluchevskiy <arseniy.sluchevskiy@gmail.com>
"""

import datetime
import string
import re
from grab.spider import Task
from grab.tools.text import normalize_space, find_number
from BaseSpider import BaseSpider
from mapping import MAPPING

class PrirodavSpider(BaseSpider):
    initial_urls = ['http://www.priroda-v.ru/']
    site_name = 'priroda-v.ru'

    def task_initial(self, grab, task):
        pq = grab.pyquery
        for a in pq.find('td.cat').eq(0).find('div a'):
            cat_name = a.text.strip()
            if cat_name in MAPPING[self.site_name]:
                url = grab.make_url_absolute(a.get('href'))
                yield Task('list', url=url, category = [cat_name])

    def task_list(self, grab, task):
        pq = grab.pyquery
        # pagination
        if pq.find('div.pages a') and pq.find('div.pages a')[-1].text == u'»»»':
            url = grab.make_url_absolute( pq.find('div.pages a')[-1].get('href') )
            yield Task('list', url=url, category=task.category)
        # items
        td_list = pq.find('td.ans')
        for i in range(len(td_list)):
            href = td_list.eq(i).children('a').attr('href')
            if not href:
                continue
            article = td_list.eq(i).find('td.art').text().split()[1]
            url = grab.make_url_absolute(href)
            item = self.db.item.find_one({'url':url, 'site':self.site_name})
            if not item:
                yield Task('page', url=url, category=task.category, article=article)
            elif not item['is_img_downloaded']:
                yield Task('image', id=item['_id'], url=item['img_url'], disable_cache=True)

    def task_page(self, grab, task):
        pq = grab.pyquery
        data = self.get_data(task)
        data['article'] = task.article
        data['name'] = pq.find('td.title').text()
        if not data['name']:
            yield task.clone(refresh_cache=True)
        data['price'] = float( pq.find('td.cost').text().strip(u'p. ').replace(' ','') )
        img_url = pq.find('td[width="34%"] div[align=center]').children('img').attr('src')
        data['img_url'] = grab.make_url_absolute( img_url )
        data['description'] = pq.find('td.art div').eq(1).text()
        if not data['description']:
            data['description'] = ''
        _id = self.db.item.save(data)
        yield Task('image', id=_id, url=data['img_url'], disable_cache=True)
