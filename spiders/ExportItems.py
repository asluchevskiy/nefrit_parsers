# -*- coding: utf-8 -*-
"""
Developed by Arseniy Sluchevskiy <arseniy.sluchevskiy@gmail.com>
"""

import logging
import pymongo
import trans
import signal
import string
import os
import csv
from PIL import Image
from models import File, Element, ElementProperty, Section, SectionElement
from mapping import MAPPING

def get_slug(name):
    return name.encode('trans/slug').replace('-','_')

class ExportItems():
    settings = None
    should_stop = False
    categories = dict()
    sub_categories = dict()

    def sigint_handler(self, signal, frame):
        logging.info('Interrupting...')
        self.should_stop = True

    def __init__(self, settings = None, site='all'):
        self.settings = settings
        self.db = pymongo.Connection()[settings['cache_db']]
        signal.signal(signal.SIGINT, self.sigint_handler)

    def _get_category(self, category_list, site):
        if site=='aromatika.su':
            cat_name = category_list[0]
            if cat_name not in self.categories:
                self.categories[cat_name] = Section.objects.get_or_create(name=cat_name, depth_level=1,
                    code=get_slug(cat_name))[0]
            if len(category_list) == 2:
                sub_cat_name = category_list[1]
                if sub_cat_name not in self.sub_categories:
                    self.sub_categories[sub_cat_name] = Section.objects.get_or_create(name=sub_cat_name, depth_level=2,
                        code=get_slug(sub_cat_name), iblock_section_id=self.categories[cat_name].id)[0]
                return self.sub_categories[sub_cat_name]
            else:
                return self.categories[cat_name]
        else:
            pass
            # TODO: use mapping

    def _save_image(self, fname):
        path = os.path.join(self.settings['images_dir'], fname)
        im = Image.open(path)
        content_type = 'image/%s' % im.format.lower()
        file_size = os.path.getsize(path)
        name = os.path.basename(fname)
        (width, height) = im.size
        subdir = 'iblock/nef/' + os.path.dirname(fname) + '/'
        f = File.objects.get_or_create(file_size=file_size, width=width, height=height, subdir=subdir,
            content_type=content_type, file_name=name, original_name=name)[0]
        return f.id

    def _save_property(self, element_id, property_id, value):
        pr = ElementProperty.objects.get_or_create(iblock_element_id=element_id, iblock_property_id=property_id, value=value)[0]
        return pr.id

    def export(self):
        for item in self.db.item.find({'is_exported':False, 'is_parsed':True, 'is_img_downloaded':True}):
#        for item in self.db.item.find({'is_parsed':True, 'is_img_downloaded':True}):
            detail_picture = self._save_image(item['img'])
            preview_picture = self._save_image(item['img_thumb'])
            category_id = self._get_category(item['category'], item['site']).id
            element = Element.objects.get_or_create(
                iblock_section_id=category_id, name=item['name'], code = get_slug(item['name']),
                preview_text=item['description'], detail_text=item['description'],
                detail_picture=detail_picture, preview_picture=preview_picture
            )[0]
            SectionElement.objects.get_or_create(iblock_element_id=element.id, iblock_section_id=category_id)
            if item['price']:
                self._save_property(element.id, 17, int(item['price']*3))
            if item['article']:
                self._save_property(element.id, 6, item['article'])
            self.db.item.update({'_id':item['_id']}, {'$set':{'is_exported':True}})
            if self.should_stop:
                break

    def _get_csv_category(self, category_list, site):
        if site=='aromatika.su':
            return category_list
        else:
            for key in MAPPING[site].keys():
                category_key = key.split('; ')
                if category_list[:len(category_key)] == category_key:
                    return [u'С других сайтов'] + MAPPING[site][key]
            return None

    def export_csv(self):
        for key in MAPPING:
            with open(os.path.join(self.settings['csv_dir'], '%s.csv' % key), 'w') as f:
                writer = csv.writer(f, delimiter=';', quoting=csv.QUOTE_ALL)
                writer.writerow([
                    'IE_NAME','IE_PREVIEW_PICTURE', 'IE_PREVIEW_TEXT', 'IE_PREVIEW_TEXT_TYPE',
                    'IE_DETAIL_PICTURE', 'IE_DETAIL_TEXT', 'IE_DETAIL_TEXT_TYPE', 'IE_CODE', 'IP_PROP17', 'IP_PROP6',
                    'IC_GROUP0', 'IC_GROUP1', 'IC_GROUP2',
                ])
    #            for item in self.db.item.find({'is_exported':False, 'is_parsed':True, 'is_img_downloaded':True, 'site':key}):
                for item in self.db.item.find({'is_parsed':True, 'is_img_downloaded':True, 'site':key}):
                    category_list = self._get_csv_category(item['category'], item['site'])
                    if not category_list:
                        continue
                    try:
                        writer.writerow(
                        [
                            item['name'],
                            '/upload/iblock/nef/' + item['img_thumb'] if item['img_url'] else '',
                            item['description'],
                            'html',
                            '/upload/iblock/nef/' + item['img'] if item['img_url'] else '',
                            item['description'],
                            'html',
                            get_slug(item['name']),
                            int(item['price']*3) if item['price'] else '',
                            item['article'] if item['article'] else '',
                        ] + category_list
                        )
                    except Exception:
                        logging.error('Can\'t export: ObjectId(\'%s\'), site %s' % (item['_id'], item['site']))
