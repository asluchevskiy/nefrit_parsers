# -*- coding: utf-8 -*-
"""
Developed by Arseniy Sluchevskiy <arseniy.sluchevskiy@gmail.com>
"""

import logging
import signal
import pymongo
import hashlib
import datetime
import os
import re
from PIL import Image
from lxml.html.clean import clean_html
from grab.tools.text import normalize_space
from grab.spider import Spider, Task

class BaseSpider(Spider):
    initial_urls = []
    settings = None
    site_name = None

    def sigint_handler(self, signal, frame):
        logging.info('Interrupting...')
        self.should_stop = True

    def prepare(self):
        signal.signal(signal.SIGINT, self.sigint_handler)
        self.setup_grab(timeout=60)

    def setup_settings(self, settings):
        self.settings = settings
        if settings.get('proxy') and not settings.get('proxy_list'):
            self.setup_grab(proxy=settings['proxy'],
                proxy_type=settings.get('proxy_type', 'http'),
                proxy_userpwd=settings.get('proxy_userpwd', ''))
        if settings.get('proxy_list'):
            self.setup_proxylist(settings['proxy_list'], 'http', auto_change=True)
        if settings.has_key('cache_db'):
            self.db = pymongo.Connection()[settings['cache_db']]

    def task_initial(self, grab, task):
        pass

    def clean_html(self, html):
        return re.sub(' *class=["\'].+?["\']', '', normalize_space(clean_html(html)))

    def get_data(self, task):
        data = dict()
        data['ctime'] = datetime.datetime.now()
        try:
            data['category'] = task.category
        except AttributeError:
            data['category'] = None
        data['site'] = self.site_name
        data['url'] = task.url
        data['is_parsed'] = True
        data['is_exported'] = False
        data['is_img_downloaded'] = False
        return data

    def task_image(self, grab, task):
        ext = grab.response.headers.get('Content-Type').split('/')[-1]
        hash = hashlib.md5(task.url).hexdigest()
        if ext not in ('jpeg', 'png', 'gif'):
            logging.error('Wrong file ext: %s' % task.url)
            return

        path_part = '%s/%s.%s' % (hash[-1], hash, ext)
        thumb_path_part = '%s/%s_thumb.%s' % (hash[-1], hash, 'jpeg')
        path = os.path.join(self.settings['images_dir'], path_part)
        thumb_path = os.path.join(self.settings['images_dir'], thumb_path_part)
        if not os.path.exists(os.path.dirname(path)):
            os.makedirs(os.path.dirname(path))
        grab.response.save(path)

        im = Image.open(path)
        if im.mode != "RGB":
            im = im.convert("RGB")
        im.thumbnail(self.settings['image_thumb_size'], Image.ANTIALIAS)
        im.save(thumb_path, 'JPEG', quality=self.settings['jpeg_quality'])

        self.db.item.update({'_id':task.id}, {'$set':{
            'is_img_downloaded' : True,
            'img' : path_part,
            'img_thumb' : thumb_path_part,
        }})
