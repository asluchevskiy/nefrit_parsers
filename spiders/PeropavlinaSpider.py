# -*- coding: utf-8 -*-
"""
Developed by Arseniy Sluchevskiy <arseniy.sluchevskiy@gmail.com>
"""

from grab.spider import Task
from pyquery import PyQuery
from BaseSpider import BaseSpider
from mapping import MAPPING

class PeropavlinaSpider(BaseSpider):
    initial_urls = ['http://www.peropavlina.ru/catalog/']
    site_name = 'peropavlina.ru'

    def task_initial(self, grab, task):
        yield Task('list', url='http://www.peropavlina.ru/catalog/', category=list(), level=1)

    def task_list(self, grab, task):
        pq = PyQuery(grab.response.body.decode('UTF-8'))

        div_list = pq.find('div.details')
        for i in range(len(div_list)):
            if not div_list.eq(i).find('h3.category_name'):
                continue
            a = div_list.eq(i).find('a')
            cat_name = a.text().split('(')[0].strip()
            url = grab.make_url_absolute(a.attr('href'))
            to_deep = False
            category = task.category + [cat_name]
            for key in MAPPING[self.site_name].keys():
                category_key = key.split('; ')[:task.level]
                if category_key == category[:len(category_key)]:
                    to_deep = True
            if to_deep:
                yield Task('list', url=url, category=category, level=task.level+1)
        for a in pq.find('ul.products li').children('a'):
            url = grab.make_url_absolute(a.get('href'))
            item = self.db.item.find_one({'url':url, 'site':self.site_name})
            if not item:
                yield Task('page', url=url, category=task.category)
            elif not item['is_img_downloaded']:
                yield Task('image', id=item['_id'], url=item['img_url'], disable_cache=True)

    def task_page(self, grab, task):
        pq = PyQuery(grab.response.body.decode('UTF-8'))
        data = self.get_data(task)
        data['name'] = pq.find('li.product a').text()
        descr = pq.find('div.description').html().strip()
        if descr == u'Нет описания для этого товара':
            descr = ''
        data['price'] = float( pq.find('div.price strong.value').text().replace(',','.') )
        img_url = pq.find('div.product_image_container').children('a').attr('href')
        data['img_url'] = grab.make_url_absolute(img_url) if img_url else None
        if not img_url:
            data['is_img_downloaded'] = True
        keys = [dt.text.strip(':') for dt in pq.find('dl dt')]
        values = [dd.text for dd in pq.find('dl dd')]
        descr_params = ''
        data['article'] = None
        if len(keys) == len(values):
            for i in range(len(keys)):
                if keys[i] == u'Артикул':
                    data['article'] = values[i]
                elif keys[i] not in (u'В коробке', u'Наличие'):
                    descr_params += u'<p>%s: %s</p>\n' % (keys[i], values[i])
        data['description'] = self.clean_html( descr_params + descr )
        _id = self.db.item.save(data)
        if data['img_url']:
            yield Task('image', id=_id, url=data['img_url'], disable_cache=True)
