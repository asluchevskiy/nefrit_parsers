# -*- coding: utf-8 -*-
"""
Developed by Arseniy Sluchevskiy <arseniy.sluchevskiy@gmail.com>
"""

import datetime
import string
import re
from grab.spider import Task
from grab.tools.text import normalize_space, find_number
from BaseSpider import BaseSpider
from mapping import MAPPING

class AromajazzSpider(BaseSpider):
    initial_urls = ['http://aromajazz.ru/catalogs']
    site_name = 'aromajazz.ru'

    def task_initial(self, grab, task):
        pq = grab.pyquery
        for a in pq.find('div.cats2 ul li a'):
            cat_name = a.text
            yield Task('list', url=a.get('href'), category = [cat_name])

    def task_list(self, grab, task):
        pq = grab.pyquery
        items_list = pq.find('div.block div.zagolovok a')
        if not items_list:
            for a in pq.find('div.catalog ul li a'):
                cat_name = a.text
                yield Task('list', url=a.get('href'), category = task.category + [cat_name])
        elif string.join(task.category, '; ') in MAPPING[self.site_name]:
            for i in range(len(items_list)):
                url = items_list.eq(i).attr('href')
                item = self.db.item.find_one({'url':url, 'site':self.site_name})
                if not item:
                    yield Task('page', url=url, category=task.category)
                elif not item['is_img_downloaded']:
                    yield Task('image', id=item['_id'], url=item['img_url'], disable_cache=True)

    def task_page(self, grab, task):
        pq = grab.pyquery
        data = dict()
        data['ctime'] = datetime.datetime.now()
        data['category'] = task.category
        data['site'] = self.site_name
        data['url'] = task.url
        data['name'] = pq.find('p.nazvanie').text()
        data['img_url'] = grab.make_url_absolute( pq.find('div.img img').attr('src') )
        data['article'] = pq.find('div.art p span').text()
        data['price'] = float( find_number(pq.find('div.art p.price').text()) )
        data['description'] = pq.find('div.t_right').remove('p.nazvanie').remove('form').html().replace('&#13;', '')
        data['description'] = re.sub('<!--.+?-->', '', normalize_space(data['description'])).strip()
        data['is_parsed'] = True
        data['is_exported'] = False
        data['is_img_downloaded'] = False
        _id = self.db.item.save(data)
        yield Task('image', id=_id, url=data['img_url'], disable_cache=True)
